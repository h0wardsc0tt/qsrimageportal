<cfscript>
	objManual = CreateObject("component", "_cfcs.Manuals");
	objManuals = objManual.getManuals();
	objManualsOrd = objManual.getManuals("Manual_Label");
	objManualsOrdPN = objManual.getManuals("Manual_Part_Number");
	
	objProduct = CreateObject("component", "_cfcs.Products");
	objProducts = objProduct.getProducts();
	objProductsOrd = objProduct.getProducts(true);
	
	objCategory = CreateObject("component", "_cfcs.Categories");
	objCategories = objCategory.getCategories();
	objCategoriesOrd = objCategory.getCategories(true);
</cfscript>