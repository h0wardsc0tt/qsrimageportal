	$(document).ready(function() {
		if($('#anchorLink-1').width()>$('#anchorLink-2').width())
		{
			$('#anchorLink-2').css('width',$('#anchorLink-1').width()+10);
			$('#anchorLink-1').css('width',$('#anchorLink-1').width()+10);
		}
		else
		{
			$('#anchorLink-1').css('width',$('#anchorLink-2').width()+10);
			$('#anchorLink-2').css('width',$('#anchorLink-2').width()+10);
		}
		
		$('.modal-dialog').draggable({
            handle: ".modal-header"
        });

		$('#confirm').on('shown.bs.modal', function (e) {
		  $("#pageContent").css({ opacity: 0.0 });
		})	
			
		$(function() {
			$('a[href*="#"]:not([href="#"])').click(function() {
			  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
				  $('html, body').animate({
					scrollTop: target.offset().top-50
				  }, 1000);
				  return false;
				}
			  }
			});
		});
	});
	
	function displayImage(imageId, imageFileName, imageDescription){
		$('#modalImage').prop('src', './getimage.cfm?id=' + imageId);
		$('#modalImage').prop('title', imageDescription);
		$('#imageTitle').html(imageDescription);
		$('#imageDownload').prop('href', './getimage.cfm?id=' + imageId);
		$('#imgModal').modal('show');
	}

	function editCategory(type, imageId, categoryId, categoryName, categoryDescription){
		$('#modalSubmitBtn').show();
		$('#adminEdit').prop('src','./admin/editCategory.cfm?type='+type+'&imageId='+imageId+'&categoryId='+
		categoryId+'&categoryName='+categoryName+'&categoryDescription='+categoryDescription);
		$('#adminEdit').css('width','300px');
		$('#adminEdit').css('height','355px');
		$('#EditCategory').modal('show');
	}

	function newCategory(type, page){
		$('#modalSubmitBtn').show();
		$('#adminEdit').prop('src','./admin/'+page+'.cfm?type='+type);
		$('#adminEdit').css('width','300px');
		$('#adminEdit').css('height','355px');
		$('#EditCategory').modal('show');
	}	
	function editThumbnail(type, imageId, categoryId, product, page){
		$('#modalSubmitBtn').show();
		$('#adminEdit').prop('src','./admin/'+page+'.cfm?type='+type+'&imageId='+imageId+'&categoryId='+categoryId+'&product='+product);
		$('#adminEdit').css('width','300px');
		$('#adminEdit').css('height','355px');
		$('#EditCategory').modal('show');
	}

	function editImage(type, imageId, categoryId, page){
		$('#modalSubmitBtn').show();
		$('#adminEdit').prop('src','./admin/'+page+'.cfm?type='+type+'&imageId='+imageId+'&categoryId='+categoryId);
		$('#adminEdit').css('width','300px');
		$('#adminEdit').css('height','355px');
		$('#EditCategory').modal('show');
	}
	
	function newProduct(type, categoryId, imageGuid){
		$('#modalSubmitBtn').show();
		$('#adminEdit').prop('src','./admin/addNewProduct.cfm?type='+type+'&categoryId='+categoryId);
		$('#adminEdit').css('width','300px');
		$('#adminEdit').css('height','355px');
		$('#EditCategory').modal('show');
	}

	function restoreProduct(type, categoryGUID){
		$('#modalSubmitBtn').show();
		$('#adminEdit').prop('src','./admin/restore.cfm?type='+type+'&categoryGUID='+categoryGUID);
		$('#adminEdit').css('width','300px');
		$('#adminEdit').css('height','355px');
		$('#EditCategory').modal('show');
	}

	function restoreCategory(type){
		$('#modalSubmitBtn').show();
		$('#adminEdit').prop('src','./admin/restore.cfm?type='+type);
		$('#adminEdit').css('width','300px');
		$('#adminEdit').css('height','355px');
		$('#EditCategory').modal('show');
	}

	function restoreLink(type, product, categoryGUID, imageId){	
		$('#modalSubmitBtn').show();
		$('#adminEdit').prop('src','./admin/restore.cfm?type='+type + '&product=' + product + '&categoryGUID=' + categoryGUID + '&imageId=' + imageId );
		$('#adminEdit').css('width','300px');
		$('#adminEdit').css('height','355px');
		$('#EditCategory').modal('show');
	}
	
	function anchorLinks(){
		if($('#anchorLinks').height()<=21)
		{
			$('#anchorLinks').height($('#anchorLinks').prop('scrollHeight'));
			$('#anchroArrow').removeClass('fa fa-plus').addClass('fa fa-minus');
			$('#anchroArrow').prop('title', 'Collapse Links');
		}
		else
		{
			$('#anchorLinks').height(20);
			$('#anchroArrow').removeClass('fa fa-minus').addClass('fa fa-plus');
			$('#anchroArrow').prop('title', 'Expand links');
		}
	}
	
	function orderProduct(type, categoryGuid){
		$('#modalSubmitBtn').show();
		$('#adminEdit').prop('src','./admin/reorderProducts.cfm?type='+type+'&categoryGuid='+categoryGuid);
		$('#adminEdit').css('width','300px');
		$('#adminEdit').css('height','355px');
		$('#EditCategory').modal('show');
	}
			
	function orderCategories(type){
		$('#modalSubmitBtn').show();
		$('#adminEdit').prop('src','./admin/reorderProducts.cfm?type='+type);
		$('#adminEdit').css('width','300px');
		$('#adminEdit').css('height','355px');
		$('#EditCategory').modal('show');
	}
			
	function orderLinks(type, product, categoryGuid){
		$('#modalSubmitBtn').show();
		$('#adminEdit').prop('src','./admin/reorderProducts.cfm?type='+type+'&product='+product+'&categoryGuid='+categoryGuid);
		$('#adminEdit').css('width','300px');
		$('#adminEdit').css('height','355px');
		$('#EditCategory').modal('show');
	}

	function checkInput(direction) {  
		var query ='Headset';  
		window.find(query, false, direction);
		return true;
	}