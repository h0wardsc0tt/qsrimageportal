$.fn.exists = function(){return this.length>0;}
var allowDelete = false;
$(document).ready(function() {
	$('#error-messages').height(0);
	$('#delete-messages').height(0);

});

var validateFileUploadUploadArray = ['jpg', 'png', 'tiff' , 'tif', 'pdf', 'eps', 'png', 'doc', 'docx', 'addImageLink'];


function validation(){
	if($('#deleteOption').exists() && $('#deleteOption').val() ==  'Yes'){
		if(!allowDelete){
			$('#modalSubmitBtn', window.parent.document).hide();
			$('#delete-box').show();
			$('#delete-messages').height($('#delete-messages').prop('scrollHeight'));
			return false;			
		}
		else{
			return true;						
		}
	}
	
	$('#error-message').html('');
	$('.expandable-element').height(0);

	if ($.inArray($('#type').val(), validateFileUploadUploadArray) >= 0){
		if(!isFileSelected()){
			$('#error-message').html('Please select a file to upload.');
			$('#error-messages').height($('#error-messages').prop('scrollHeight'));
			
			return false;
		}		
		return true;
	}
	
	if ($('#type').val() == 'thumbnail'){
		if(!isFileSelected() && !isValue('imageDescription')){
			$('#error-message').html('Please select a file to upload or enter a value for "' + $('#imageDescription').prop('placeholder') + '"');
			$('#error-messages').height($('#error-messages').prop('scrollHeight'));
			
			return false;
		}		
		return true;
	}
	
	if ($('#type').val() == 'addNewProduct'){
		if(!isFileSelected() || !isValue('imageDescription')){
			$('#error-message').html('Please select a file to upload and enter a value for "' + $('#imageDescription').prop('placeholder') + '"');
			$('#error-messages').height($('#error-messages').prop('scrollHeight'));
			
			return false;
		}		
		return true;
	}	
	
	if ($('#type').val() == 'newCategory'){
		if(!isFileSelected() || !isValue('categoryDescription') || !isValue('categoryName')){
			$('#error-message').html('Please select a file to upload and enter a value for "' +
				 $('#categoryDescription').prop('placeholder') + '" and ' + 
				 $('#categoryName').prop('placeholder') + '"');
			$('#error-messages').height($('#error-messages').prop('scrollHeight'));
			
			return false;
		}		
		return true;
	}	

	return true;
}

function validateOrderPage(){
	$('#error-message').html('');
	$('.expandable-element').height(0);
	
	var duplicateArray = []
	var orderChanges = '';
	$('#productList :input, select').each(
		function(index){  
			var input = $(this);
			var selected = $(this).find('option:selected');
		   	//console.log(selected.text()); 
		   	//console.log(selected.val()); 
		   	//console.log(selected.data('foo')+'   new='+selected.val()); 
		   	duplicateArray.push(input.val());
            if((index + 1) != input.val()){
                 orderChanges += (orderChanges != '' ? ',' : '') + '(' + (index + 1) + ',' + input.val() + ')';
			}
		}
	);

	if(hasDuplicates(duplicateArray)){
		switch($('#type').val()){
			case 'reOrderProduct':
				$('#error-message').html('You have products with the same sequence order. Please correct the duplicate order sequences.');
				break;
			case 'orderCategory':
				$('#error-message').html('You have categories with the same sequence order. Please correct the duplicate order sequences.');
				break;
			case 'orderLinks':
				$('#error-message').html('You have image links with the same sequence order. Please correct the duplicate order sequences.');
				break;
		}
		$('#error-messages').height($('#error-messages').prop('scrollHeight'));
		$("html, body").animate({ scrollTop: $(document).height() }, "slow");
		return false;
	}
	if(orderChanges == ''){
		$('#error-message').html('You have not made any sequence changes.');
		$('#error-messages').height($('#error-messages').prop('scrollHeight'));
		$("html, body").animate({ scrollTop: $(document).height() }, "slow");
		return false;		
	}
	$('#productSequenceChanges').val(orderChanges);
	return true;
}

function isFileSelected(){
	if($('#photo').val() == ''){
		return false;					
	}
	else
		return true
}

function isValue(elm){
	if($('#' + elm).val() == ''){
		return false;					
	}
	else
		return true
}

function hasDuplicates(array) {
	return (new Set(array)).size !== array.length;
}

function validateRestore(){
	if(!$("input:radio[name='category']").is(":checked")){
		$('#error-messages').height($('#error-messages').prop('scrollHeight'));  
		console.log('nck');
		return false;
	}
	return true;;
}

function cancelImageDelete(){
	$('#modalSubmitBtn', window.parent.document).show();
	$('#delete-box').hide();
	$('#delete-messages').height(0);

}

function allowImageDelete(){
	allowDelete = true;
	$('#submit').click();
}
	