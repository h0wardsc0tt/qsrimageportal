
<cfoutput>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>#title#</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="../css/dashboard.css" rel="stylesheet">
    <link href="../css/qsrImagePortalAdmin.css" rel="stylesheet">
</head>
<body>
	<h4 style="text-align:center;">
		#title#
	</h4>
	<cfif FORM.submitted && status eq "OK">
        <div class="container-fluid" style="min-height: 100%;">
            <p class="row placeholders">
                <!---<cfswitch expression="#type#">  
                    <cfcase value="reOrderProduct">  
               			 Your request to reorder the "#title#" category was successful.
                    </cfcase>   
                    <cfcase value="orderCategory">  
                 		Your request to reorder the categories was successful.
                    </cfcase>   
                    <cfcase value="orderLinks">  
                 		Your request to reorder the links was successful.
                    </cfcase>   
                </cfswitch>--->
            </p >
            <p class="row placeholders">
                You will need to refresh the page to see the change.
            </p >
            <p class="row placeholders">
                <a href="javascript:parent.location.reload();">Click here to refresh the page</a>
            </p >
        </div>
    <cfelse>
         <form
            action="#CGI.script_name#"
            method="post"
            onSubmit="return validateOrderPage();"
            > 
            <section id="productList">
                <div class="container">
                    <CFLOOP QUERY="ProductOrder">
                            <div class="row ">
                                <div class="col-xs-8" style="text-align:left;">
                                    <cfswitch expression="#type#">  
                                        <cfcase value="reOrderProduct">  
                                            #imagedescription#
                                        </cfcase>   
                                        <cfcase value="orderCategory">  
                                        	#categoryDescription#
                                        </cfcase>   
                                        <cfcase value="orderLinks">  
                                            #imagedescription#
                                        </cfcase>   
									</cfswitch>
								</div>
                                <div class="col-xs-4" style="padding-bottom:10px;">
                                    <select name="orderOption" id="orderOption" class="form-control order" style="width:70px"> 
                                        <CFLOOP index="i" from="1" to=#ProductOrder.RecordCount#>
                                            <cfswitch expression="#type#">  
                                                <cfcase value="reOrderProduct">  
                                             		<option value="#i#" data-foo="#productOrder#" <cfif productOrder eq  i>selected</cfif>>#i#</option> 
                                                </cfcase>   
                                                <cfcase value="orderCategory">  
                                             		<option value="#i#" data-foo="#productOrder#" <cfif productOrder eq  i>selected</cfif>>#i#</option> 
                                                </cfcase>   
                                                <cfcase value="orderLinks">  
                                             		<option  value="#i#" data-foo="#productOrder#" <cfif productOrder eq  i+1>selected</cfif>>#i#</option> 
                                                </cfcase>   
                                            </cfswitch>
                                        </CFLOOP>
                                    </select>
                                </div>  
                            </div>     				
                    </CFLOOP>        
                </div>
            </section>
            <div style="text-align:center;vertical-align:top;height:50px; margin: 0px 10px 0px 10px;" id="error-messages" class="recovery-content-row expandable-element"> 
                <div class="system-message" id="error-message"></div>
            </div>
            
            <input type="hidden" name="submitted" value="true" />
            <input type="hidden" name="id" value="#FORM.id#" />            
            <input type="hidden" name="categoryGuid" value="#URL.categoryGuid#" />
            <input type="hidden" id="type" name="type" value="#type#" />
            <input type="hidden" id="title" name="title" value="#title#" />
            <input type="hidden" id="productSequenceChanges"  name="productSequenceChanges" value="" /> 
            <input type="hidden" id="product"  name="product" value="#URL.product#" /> 
            <input type="submit" 
                    class="btn btn-primary"
                    value="Submitr"
                    id="submit"
                    style="display:none;" 
                    />
               
        </form>
	</cfif>
	<!-- Placed at the end of the document so the pages load faster --> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="../js/qsrImagePortalAdmin.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		<cfif FORM.submitted && status eq "OK">
			$('##modalSubmitBtn', window.parent.document).hide();
		<cfelse>
			function submitForm(){
				$('##submit').click();		
			}	
		</cfif>	
	</script>       
	<style type="text/css">
		.placeholder:last-child {
			margin-bottom:5px;
		}
	</style>
</body>
</html>
</cfoutput>