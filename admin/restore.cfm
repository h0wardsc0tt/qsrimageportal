<cfsetting showdebugoutput="no">
<cfinclude template="../serverSettings.cfm">
<cfparam name="FORM.Page_UID" default="">
<cfif not StructKeyExists(SESSION,"IsLoggedIn")>
	<cfabort>
</cfif>
<!--- Param form data. --->
<cfparam name="FORM.id" type="numeric" default="0" />

<cfparam name="FORM.imageType" type="string" default="" />
<cfparam name="FORM.imageId" type="string" default="" />
<cfparam name="FORM.categoryId" type="string" default="" />
<cfparam name="FORM.submitted" type="boolean" default="false" />
<cfparam name="FORM.type" type="string" default="" />
<cfparam name="FORM.categoryGuid" type="string" default="" />
<cfparam name="FORM.product" type="string" default="" />
<cfparam name="FORM.title" type="string" default="" />
<cfparam name="FORM.category" type="string" default="" />

<cfparam name="URL.imageId" default="">
<cfparam name="URL.type" default="">
<cfparam name="URL.categoryGuid" default="">
<cfparam name="URL.product" default="">

<cfset type = "">
              
<cfscript>
	object = CreateObject("component", "#cfcs#.ImagePortal");  
</cfscript> 

<!--- Check to see if the form as been submitted. --->
<cfif FORM.submitted>

	<cfswitch expression="#FORM.Type#">  
        <cfcase value="restoreCategory"> 
    		<cfset title = "Restored Deleted Category"> 
            <cfscript> 
                ret = getSelect = object.setCategoryToActive(
					categoryGuid = FORM.category
				);
            </cfscript>
			<cfset status = #ret#> 
            <cfif status neq "OK">
                <cfset message = "An error occured, please retry again.">
                <cfset disableButton = "">
            </cfif>
        </cfcase>   
        <cfcase value="restoreProduct">  
   			<cfset title = "Restored Deleted Product"> 
			<cfscript> 
				ret = getSelect = object.SetProductToIsActive(
					categoryGuid = FORM.categoryGuid,
					product = FORM.category
				);
            </cfscript>
			<cfset status = #ret#> 
			<cfif status neq "OK">
				<cfset message = "An error occured, please retry again.">
				<cfset disableButton = "">
			</cfif>
        </cfcase>   
        <cfcase value="restoreLinks">
   			<cfset title = "Restored Deleted Link"> 
			<cfscript> 
				ret = getSelect = object.setLinkToActive(
					Guid = FORM.category
				);
            </cfscript>
			<cfset status = #ret#> 
			<cfif status neq "OK">
				<cfset message = "An error occured, please retry again.">
				<cfset disableButton = "">
			</cfif>
        </cfcase>   
 	</cfswitch>

<cfelse>
	<cfswitch expression="#URL.Type#">  
        <cfcase value="restoreCategory">  
    		<cfset title = "Restore Deleted Category"> 
            <cfscript> 
                getSelect = object.getDeletedCategories();
            </cfscript>
        </cfcase>   
        <cfcase value="restoreProduct">  
   			<cfset title = "Restore Deleted Product"> 
            <cfscript> 
                getSelect = object.getDeletedProducts(
					categoryGuid = URL.categoryGuid
				);
            </cfscript>
        </cfcase>   
        <cfcase value="restoreLinks">
   			<cfset title = "Restore Deleted Link"> 
            <cfscript> 
                getSelect = object.getDeletedLinks(
					categoryGuid = URL.categoryGuid,
					product = URL.product
				);
            </cfscript>           
        </cfcase>   
 	</cfswitch>
    <cfset Categories = getSelect>   
    <cfset FORM.categoryGUID = URL.categoryGUID>
    <cfset FORM.product = URL.product>
    <cfset FORM.type = URL.type>
    <cfset type = URL.Type> 
    <cfset FORM.imageId = URL.imageId>
</cfif>

<cfinclude template="restorePage.cfm">







