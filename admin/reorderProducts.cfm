<cfsetting showdebugoutput="no">
<cfinclude template="../serverSettings.cfm">
<cfparam name="FORM.Page_UID" default="">
<cfif ReFindNoCase("([0-9A-Z]){32}", FORM.Page_UID) GT 0>
	<cfabort>
</cfif>
<!--- Param form data. --->
<cfparam name="FORM.id" type="numeric" default="0" />

<cfparam name="FORM.imageType" type="string" default="" />
<cfparam name="FORM.imageId" type="string" default="" />
<cfparam name="FORM.categoryId" type="string" default="" />
<cfparam name="FORM.categoryGuid" type="string" default="" />
<cfparam name="FORM.submitted" type="boolean" default="false" />
<cfparam name="FORM.photo" type="string" default="" />
<cfparam name="FORM.type" type="string" default="" />
<cfparam name="FORM.deleteOption" type="string" default="No" />
<cfparam name="FORM.imageDescription" type="string" default="No" />
<cfparam name="FORM.title" type="string" default="No" />
<cfparam name="FORM.productSequenceChanges" type="string" default="" />
<cfparam name="FORM.product" type="string" default="" />

<cfparam name="URL.imageId" default="">
<cfparam name="URL.type" default="">
<cfparam name="URL.categoryGuid" default="">
<cfparam name="URL.product" default="">

<cfset message = "Update Successful.">
<cfset disableButton = "disabled">
<cfset type = "">
<cfset title = ""> 
<cfset isActiveOptionText = "">   
<cfset imageType = "">
<cfset imageAnchorText = "">
<cfset selectDescription = "">

<cfset categoryGuid = #URL.categoryGuid#>              
<cfscript>
	object = CreateObject("component", "#cfcs#.ImagePortal");  
</cfscript> 

<!--- Check to see if the form as been submitted. --->
<cfif FORM.submitted>
	<cfset title = FORM.title> 
   	<cfset type = FORM.type> 
    <cfswitch expression="#type#">  
        <cfcase value="reOrderProduct">  
            <cfscript> 
                ret = object.UpdateProductOrder
                (
                    categoryGuid = FORM.categoryGuid,
                    orderPair = FORM.productSequenceChanges
                );
             </cfscript>
        </cfcase>   
        <cfcase value="orderCategory">  
            <cfscript> 
                ret = object.UpdateCategoryOrder
                (
                    categoryGuid = FORM.categoryGuid,
                    orderPair = FORM.productSequenceChanges
                );
            </cfscript>
        </cfcase>   
        <cfcase value="orderLinks">    
            <cfscript> 
                ret = object.UpdateLinkOrder
                (
                    categoryGuid = FORM.categoryGuid,
                    orderPair = FORM.productSequenceChanges,
                    product = FORM.product
                );
            </cfscript>
        </cfcase>   
    </cfswitch>    
    <cfset status = #ret#> 
    <cfif status neq "OK">
        <cfset message = "An error occured, please retry again.">
    </cfif>
<cfelse>
	<cfset type = URL.Type> 
    <cfswitch expression="#type#">  
        <cfcase value="reOrderProduct">  
			<cfscript> 
                getSelect = object.DisplayProductOrder
                (
                    categoryGuid = categoryGuid
                );
            </cfscript>
    		<cfset ProductOrder = getSelect>
    		<cfset title = "Order " & getSelect.CategoryDescription> 
    	</cfcase>   
        <cfcase value="orderCategory">  
			<cfscript> 
                getSelect = object.DisplayCategoryOrder();     
            </cfscript>
    		<cfset ProductOrder = getSelect>
    		<cfset title = "Change Category Order"> 
   		</cfcase>   
        <cfcase value="orderLinks">  
			<cfscript> 
                getSelect = object.DisplayLinkOrder
				(
                    categoryGuid = categoryGuid,
					product = product
				);     
            </cfscript>
    		<cfset ProductOrder = getSelect>
    		<cfset title = "Change Link Order"> 
   		</cfcase>   
	</cfswitch>

	<cfset message = "">
    <cfset type = URL.Type> 
</cfif>

<cfinclude template="adminReorderPage.cfm">



