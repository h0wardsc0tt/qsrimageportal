<cfsetting showdebugoutput="no">
<cfinclude template="../serverSettings.cfm">
<cfparam name="FORM.Page_UID" default="">
<cfif ReFindNoCase("([0-9A-Z]){32}", FORM.Page_UID) GT 0>
	<cfabort>
</cfif>
<!--- Param form data. --->
<cfparam name="FORM.id" type="numeric" default="0" />

<cfparam name="FORM.imageType" type="string" default="" />
<cfparam name="FORM.imageId" type="string" default="" />
<cfparam name="FORM.categoryId" type="string" default="" />
<cfparam name="FORM.submitted" type="boolean" default="false" />
<cfparam name="FORM.photo" type="string" default="" />
<cfparam name="FORM.type" type="string" default="" />
<cfparam name="FORM.deleteOption" type="string" default="No" />
<cfparam name="FORM.imageDescription" type="string" default="No" />
<cfparam name="FORM.description" type="string" default="No" />
<cfparam name="FORM.categoryDescription" type="string" default="No" />
<cfparam name="FORM.categoryName" type="string" default="No" />
<cfparam name="URL.imageId" default="">
<cfparam name="URL.type" default="">
<cfparam name="URL.categoryId" default="">
<cfparam name="URL.categoryName" default="">
<cfparam name="URL.categoryDescription" default="">

<cfset message = "Update Successful.">
<cfset disableButton = "disabled">
<cfset type = "">
<cfset title = ""> 
<cfset isActiveOptionText = "">   
<cfset imageType = "">
<cfset imageAnchorText = "">
              
<cfscript>
	object = CreateObject("component", "#cfcs#.ImagePortal");  
</cfscript> 
<!--- Check to see if the form as been submitted. --->
<cfif FORM.submitted>
	<cfif #FORM.deleteOption# eq "Yes">    
	   <cfscript> 
            ret = object.setCategoryIsActive
            (
                categoryGuid = FORM.categoryId
            );
        </cfscript>
        <cfset status = #ret#> 
        <cfif status neq "OK">
            <cfset message = "An error occured, please retry again.">
            <cfset disableButton = "">
        </cfif>
	<cfelse>
		<cfset type = FORM.type>
        <cfif Len( FORM.photo )>
        	<cfset createThumbnail = false> 
            <cfinclude template="../cfincludes/fileUpload.cfm">    
            <cfscript> 
                ret = object.updateCategoryImage
                (
                    image = binPhoto, 
                    categoryGuid = FORM.categoryId, 
                    categoryDescription = FORM.categoryDescription,
					categoryName = FORM.categoryName,
                    imageName = CFFILE.clientFileName, 
                    imagePrefix = LCase(CFFILE.clientFileExt)
               );
            </cfscript>
            <cfset status = #ret#>
            <cfif status neq "OK">
                <cfset message = "An error occured, please retry again.">
                <cfset disableButton = "">
            </cfif> 
        <cfelse>
            <cfscript> 
                ret = object.updateCategory(categoryGuid = FORM.categoryId, categoryDescription = FORM.categoryDescription, categoryName = FORM.categoryName);
            </cfscript>
            <cfset status = #ret#> 
            <cfif status neq "OK">
                <cfset message = "An error occured, please retry again.">
                <cfset disableButton = "">
            </cfif> 
        </cfif>
	</cfif>        
<cfelse>
	<cfset message = "">
    <cfset disableButton = "">
    <cfset FORM.categoryId = URL.categoryId>
    <cfset type = URL.Type> 
    <cfset FORM.imageId = URL.imageId>
    <cfset FORM.categoryName = URL.categoryName>
    <cfset FORM.categoryDescription = URL.categoryDescription>
</cfif>

<cfinclude template="adminPage.cfm">



