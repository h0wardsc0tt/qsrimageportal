<cfsetting showdebugoutput="no">
<cfinclude template="../serverSettings.cfm">
<!--- Param form data. --->
<cfparam name="FORM.Page_UID" default="">
              
<cfif ReFindNoCase("([0-9A-Z]){32}", FORM.Page_UID) GT 0>
	<cfabort>
</cfif>

<cfparam name="FORM.id" type="numeric" default="0" />
<cfparam name="FORM.imageId" type="string" default="" />
<cfparam name="FORM.categoryId" type="string" default="" />
<cfparam name="FORM.submitted" type="boolean" default="false" />
<cfparam name="FORM.photo" type="string" default="" />
<cfparam name="FORM.imageDescription" type="string" default="No" />
<cfparam name="FORM.description" type="string" default="No" />
<cfparam name="deleteOption" type="string" default="No" />
<cfparam name="URL.imageId" default="">
<cfparam name="URL.type" default="">
<cfparam name="URL.categoryId" default="">
<cfparam name="URL.product" default="">

<cfset message = "Update Successful.">
<cfset disableButton = "disabled">
<cfset type = "">
<cfset title = ""> 
<cfset isActiveOptionText = "">   

<cfscript>
	object = CreateObject("component", "#cfcs#.ImagePortal");  
</cfscript> 

<!--- Check to see if the form as been submitted. --->
<cfif FORM.submitted>
	<cfif #FORM.deleteOption# eq "Yes">    
	   <cfscript> 
            ret = object.setProductIsActive
            (
                imageGuid = FORM.imageId
            );
        </cfscript>
        <cfset status = #ret#> 
        <cfif status neq "OK">
            <cfset message = "An error occured, please retry again.">
            <cfset disableButton = "">
        </cfif>
	<cfelse>
		<cfset type = FORM.type>
        <cfif Len( FORM.photo )>
        	<cfset createThumbnail = true> 
            <cfinclude template="../cfincludes/fileUpload.cfm">    
            <cfscript> 
                ret = object.updateImageAndDescrption
                (
                    image = binPhoto, 
                    imageGuid = FORM.imageId, 
                    imageDescription = FORM.imageDescription
                );
            </cfscript>
            <cfset status = #ret#> 
            <cfif status neq "OK">
                <cfset message = "An error occured, please retry again.">
                <cfset disableButton = "">
            </cfif>
        <cfelse>
            <cfscript> 
                ret = object.updateImageDescription(imageGuid = FORM.imageId, imageDescription = FORM.imageDescription);
            </cfscript>
            <cfset status = #ret#> 
            <cfif status neq "OK">
                <cfset message = "An error occured, please retry again.">
                <cfset disableButton = "">
            </cfif> 
        </cfif>
	</cfif>
<cfelse>
    <cfset type =  URL.Type>
	<cfset message = "">
    <cfset disableButton = "">
   	<cfset FORM.categoryId = URL.categoryId>
    <cfset FORM.imageDescription = URL.product>
    <cfset FORM.imageId = URL.imageId>
</cfif>

<cfinclude template="adminPage.cfm">



