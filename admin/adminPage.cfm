<cfset updateStatus = "">
<cfset deleteType = "">
<cfswitch expression="#type#">  
    <cfcase value="addNewProduct">  
        <cfset title = "Add A New Product">    
        <cfset updateStatus = "Your request to add a new Product was successful.">    
    </cfcase>   
    <cfcase value="addImageLink">  
        <cfset title = "Add A New Image Link">
        <cfset isActiveOptionText = "">     
        <cfset updateStatus = "Your request to add a new Image Link was successful.">    
    </cfcase> 
    <cfcase value="thumbnail">  
        <cfset title = "#FORM.imageDescription#"> 
        <cfset isActiveOptionText = "Delete this product">    
        <cfset updateStatus = "Your request to add a thumbnail was successful.">    
		<cfset deleteType = "Product">
    </cfcase>     
    <cfcase value="category">  
        <cfset title = "Edit Category"> 
        <cfset isActiveOptionText = "Delete Category">    
        <cfset updateStatus = "Your request to edit the Category was successful.">    
    </cfcase>     
    <cfcase value="newCategory">  
        <cfset title = "Add a New Category"> 
        <cfset isActiveOptionText = "">    
        <cfset updateStatus = "Your request to add a new Category was successful.">    
    </cfcase>     
    <cfdefaultcase> 
    	<cfif #FORM.deleteOption# eq "No">  
        	<cfset title = "#object.getLinkType(type)#">   
            <cfswitch expression="#type#">  
                <cfcase value="jpg,tif,tiff,png,eps"> 
                    <cfset isActiveOptionText = "Delete #type# Image Link">       
                 </cfcase> 
                <cfcase value="doc,docx"> 
                    <cfset isActiveOptionText = "Delete Word Document Link">        
                </cfcase> 
                <cfcase value="pdf"> 
                    <cfset isActiveOptionText = "Delete PDF Document Link">         
                </cfcase> 
           </cfswitch> 
        <cfelse>  
            <cfswitch expression="#Form.type#">  
                <cfcase value="jpg,tif,tiff,png,eps,doc,docx,pdf"> 
        			<cfset title = "#object.getLinkType(type)#">   
                 </cfcase> 
              	<cfcase value="thumbnail"> 
					<cfset title = "Delete '#Form.title#' Product">    
					<cfset updateStatus = "Your request to delete '#title#' product was successful.">    		
              	</cfcase> 
              	<cfcase value="category"> 
					<cfset title = "Category Deleted">    
					<cfset updateStatus = "Your request to delete '#title#' category was successful.">    		
				</cfcase>
				<cfdefaultcase> 
					<cfset updateStatus = "Your request to delete the '#title#' image link was successful.">    		
    			</cfdefaultcase>  
            </cfswitch> 
       </cfif>  
    </cfdefaultcase>  
</cfswitch> 

<cfoutput>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>#title#</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="../css/dashboard.css" rel="stylesheet">
    <link href="../css/qsrImagePortalAdmin.css" rel="stylesheet">

</head>
<body>

	<h4 style="text-align:center;">
		#title#
	</h4>
	<cfif FORM.submitted && status eq "OK">
        <div class="container-fluid" style="min-height: 100%;">
            <p class="row placeholders">
                	<!---#updateStatus#--->
            </p >
            <p class="row placeholders">
                You will need to refresh the page to see the change.
            </p >
            <p class="row placeholders">
                <a href="javascript:parent.location.reload();">Click here to refresh the page</a>
            </p >
         </div>
    <cfelse>
        <form
            action="#CGI.script_name#"
            method="post"
            enctype="multipart/form-data"
            onSubmit="return validation();"
            >
    
            <input type="hidden" name="submitted" value="true" />
            <input type="hidden" name="id" value="#FORM.id#" />            
            <input type="hidden" name="imageId" value="#FORM.imageId#" />
            <input type="hidden" name="categoryId" value="#FORM.categoryId#" />
            <input type="hidden" id="type" name="type" value="#type#" />
            <input type="hidden" name="description" value="#FORM.description#" />
            <input type="hidden" name="title" value="#title#" />
   
            <cfif type eq "category" || type eq "newCategory">                                 
               <p>
                    <input
                        type="text"
                        name="categoryDescription"
                        id="categoryDescription"
                        class="form-control"
                        maxlength="100"
                        style="width:100%;"
                        maxlength="150"
                        value="#FORM.categoryDescription#"
                        placeholder="Category Description"
                        />
                </p>
               <p>
                    <input
                        type="text"
                        name="categoryName"
                        id="categoryName"
                        class="form-control"
                        maxlength="100"
                        style="width:100%;"
                        maxlength="150"
                        value="#FORM.categoryName#"
                        placeholder="Category Name"
                        />
                </p>
               
            </cfif>
          
            <cfif type eq "addNewProduct" || type eq "thumbnail">                                 
               <p>
                    <input
                        type="text"
                        name="imageDescription"
                        id="imageDescription"
                        class="form-control"
                        maxlength="100"
                        style="width:100%;"
                        maxlength="150"
                        value="#FORM.imageDescription#"
                        placeholder="Product Description"
                        />
                </p>
            </cfif>
               
            <p>
                Image:<br />
                <input
                    type="file"
                    name="photo"
                    size="50"
                    id="photo"
                    />
            </p>
            
            <cfif #isActiveOptionText# neq "">
            <p>
                <select name="deleteOption" id="deleteOption"class="form-control" style="width:100%;"> 
                    <option value="No" selected>#isActiveOptionText# ? No</option> 
                    <option value="Yes">#isActiveOptionText# ? Yes</option> 
                </select> 
            </p>
            </cfif>
    

                <input #disableButton#
                    type="submit" 
                    class="btn btn-primary"
                    value="Submit"
                    id="submit"
                    style="text-align:center; width:100%; display:none;" 
                    />

    
			<div style="text-align:center;vertical-align:top;" id="error-messages" class="recovery-content-row expandable-element" style="height:0;"> 
				<div class="system-message" id="error-message"></div>
			</div>
    		
            <div style="text-align:center;vertical-align:top;" id="delete-messages" class="recovery-content-row expandable-element" style="height:0;">               
	            <div id="delete-box" class="rounded-corners confirm"  style="display:none;">
			        <div class="system-message" id="delete-message">Are you sure you want to delete this #deleteType#?</div>
			        <div  style="text-align:right; padding: 10px 10px 0px 0px; vertical-align: bottom;">
		            	<button id="delete" type="button" class="btn btn-primary" onclick='allowImageDelete();'>Yes</button>
		            	<button id="cancel" type="button" class="btn btn-default" onclick='cancelImageDelete();'>Cancel</button>
	            	</div>
	            </div>

            </div> 
                       
           <p style="color:red; text-align:center;">
                #message#
            </p>

     
        </form>
	</cfif>
	<!-- Placed at the end of the document so the pages load faster --> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="../js/qsrImagePortalAdmin.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		<cfif FORM.submitted && status eq "OK">
			$('##modalSubmitBtn', window.parent.document).hide();
		<cfelse>
			function submitForm(){
				$('##submit').click();		
			}	
		</cfif>	
	</script>       
	<style type="text/css">
	</style>
</body>
</html>
</cfoutput>