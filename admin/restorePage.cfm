
<cfoutput>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<!---<title>#title#</title>--->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="../css/dashboard.css" rel="stylesheet">
    <link href="../css/qsrImagePortalAdmin.css" rel="stylesheet">

</head>
<body>

	<h4 style="text-align:center;">
			#title#
	</h4>
	<cfif FORM.submitted && status neq "OK">
        <div class="container-fluid" style="min-height: 100%;">
            <p class="row placeholders">
            	An error has occured.
            </p>
            <p class="row placeholders">
            	<cfdump var="#status#">
            </p>
        </div>
	<cfelseif FORM.submitted && status eq "OK">
        <div class="container-fluid" style="min-height: 100%;">
            <p class="row placeholders">
                <!---<cfswitch expression="#FORM.type#">  
                    <cfcase value="restoreCategory">  
               			 Your request to restore a category was successful.
                    </cfcase>   
                    <cfcase value="restoreProduct">  
                 		Your request to restore the product was successful.
                    </cfcase>   
                    <cfcase value="restoreLinks">  
                 		Your request to restore the links was successful.
                    </cfcase>   
                </cfswitch>--->
            </p >
            <p class="row placeholders">
                You will need to refresh the page to see the change.
            </p >
            <p class="row placeholders">
                <a href="javascript:parent.location.reload();">Click here to refresh the page</a>
            </p >
        </div>
    <cfelse>
        <form
            action="#CGI.script_name#"
            method="post"
            onSubmit="return validateRestore();"
            >	
            <input type="hidden" name="submitted" value="true" />
            <input type="hidden" name="id" value="#FORM.id#" />            
            <input type="hidden" name="imageId" value="#FORM.imageId#" />
            <!---<input type="hidden" name="categoryId" value="#FORM.categoryId#" />--->
            <input type="hidden" name="categoryGuid" value="#FORM.categoryGUID#" />
            <input type="hidden" name="Product" value="#FORM.product#" />
            <input type="hidden" id="type" name="type" value="#type#" />

            <input type="submit" 
                    class="btn btn-primary"
                    value="Submit"
                    id="submit"
                    style="display:none;" 
                    />
 			<section>  
                <div class="container">
                    <cfswitch expression="#URL.Type#">  
                        <cfcase value="restoreCategory">
                       		<cfif Categories.RecordCount>
	                            <CFLOOP QUERY="Categories">
	                                <div class="row" style="margin-bottom:10px;">
	                                    <div class="col-xs-2" style="text-align:left;"> 
	                                        <input type="radio" name="category" value="#categoryGuid#">
	                                    </div>
	                                     <div class="col-xs-10" style="text-align:left;"> 
	                                        #categoryDescription#
	                                    </div>                         
	                                </div>
	                            </CFLOOP>
                        	<cfelse>
						       <div class="container-fluid" style="min-height: 100%;">
						            <p class="row placeholders">
						            	There are no Categories to recover.
						            </p>
						        </div>
                        	</cfif>
                        </cfcase>   
                        <cfcase value="restoreProduct">  
							<cfif Categories.RecordCount>
	                           <CFLOOP QUERY="Categories">
	                                <div class="row" style="margin-bottom:10px;">
	                                    <div class="col-xs-2" style="text-align:left;"> 
	                                        <input type="radio" name="category" value="#product#">
	                                    </div>
	                                     <div class="col-xs-10" style="text-align:left;"> 
	                                        #ImageDescription#
	                                    </div>                         
	                                </div>
	                           </CFLOOP> 
                         	<cfelse>
						       <div class="container-fluid" style="min-height: 100%;">
						            <p class="row placeholders">
						            	There are no Products to recover.
						            </p>
						        </div>
                        	</cfif>
                        </cfcase>   
                        <cfcase value="restoreLinks">  
							<cfif Categories.RecordCount>
								<CFLOOP QUERY="Categories">
									<div class="row" style="margin-bottom:10px;">
									    <div class="col-xs-2" style="text-align:left;"> 
									        <input type="radio" name="category" value="#guid#">
									    </div>
									     <div class="col-xs-10" style="text-align:left;"> 
									        #imageAnchorText#<br>File:#ImageName#
									    </div>                         
									 </div>
								</CFLOOP> 
                        	<cfelse>
							       <div class="container-fluid" style="min-height: 100%;">
							            <p class="row placeholders">
							            	There are no Image Links to recover.
							            </p>
							       </div>
                        	</cfif>
                      </cfcase>   
                    </cfswitch>                
                  </div>        
            </section>   
	        <div style="text-align:center;vertical-align:top;" id="error-messages" class="recovery-content-row expandable-element" style="height:0;"> 
	            <div class="system-message" id="error-message">Please select an item to restore.</div>
	        </div>
        </form>

	</cfif>
	<!-- Placed at the end of the document so the pages load faster --> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="../js/qsrImagePortalAdmin.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		<cfif FORM.submitted || not Categories.RecordCount>
			$('##modalSubmitBtn', window.parent.document).hide();
		<cfelse>
			function submitForm(){
				$('##submit').click();		
			}	
		</cfif>	
	</script>       
	<style type="text/css">
	</style>
</body>
</html>
</cfoutput>