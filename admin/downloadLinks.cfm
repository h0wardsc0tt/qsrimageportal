<cfsetting showdebugoutput="no">
<cfinclude template="../serverSettings.cfm">
<cfparam name="FORM.Page_UID" default="">
<cfif ReFindNoCase("([0-9A-Z]){32}", FORM.Page_UID) GT 0>
	<cfabort>
</cfif>
<!--- Param form data. --->
<cfparam name="FORM.id" type="numeric" default="0" />

<cfparam name="FORM.imageType" type="string" default="" />
<cfparam name="FORM.imageId" type="string" default="" />
<cfparam name="FORM.categoryId" type="string" default="" />
<cfparam name="FORM.submitted" type="boolean" default="false" />
<cfparam name="FORM.photo" type="string" default="" />
<cfparam name="FORM.type" type="string" default="" />
<cfparam name="FORM.deleteOption" type="string" default="No" />
<cfparam name="FORM.imageDescription" type="string" default="No" />
<cfparam name="FORM.description" type="string" default="No" />

<cfparam name="URL.imageId" default="">
<cfparam name="URL.type" default="">
<cfparam name="URL.categoryId" default="">

<cfset message = "Update Successful.">
<cfset disableButton = "disabled">
<cfset type = "">
<cfset title = ""> 
<cfset isActiveOptionText = "">   
<cfset imageType = "">
<cfset imageAnchorText = "">
              
<cfscript>
	object = CreateObject("component", "#cfcs#.ImagePortal");  
</cfscript> 

<cfset type = FORM.type>

<!--- Check to see if the form as been submitted. --->
<cfif FORM.submitted>
	<cfif #FORM.deleteOption# eq "Yes">    
	   <cfscript> 
            ret = object.setIsActive
            (
                imageGuid = FORM.imageId
            );
        </cfscript>
        <cfset status = #ret#> 
        <cfif status neq "OK">
            <cfset message = "An error occured, please retry again.">
            <cfset disableButton = "">
        </cfif>
	<cfelse>
        <cfif Len( FORM.photo )>
        	<cfset createThumbnail = false>        	
            <cfinclude template="../cfincludes/fileUpload.cfm">    
            <cfscript> 
                linkType = object.getLinkType(LCase(CFFILE.clientFileExt));
            </cfscript>
            <cfset imageAnchorText = #linkType#> 
            <cfscript> 
                imageType = object.getImageType(imageAnchorText);
            </cfscript>
            <cfscript> 
                ret = object.updateProductLink
                (
                    image = binPhoto, 
                    imageType = imageType, 
                    imageAnchorText = imageAnchorText, 
                    imageName = CFFILE.clientFileName, 
                    imagePrefix = LCase(CFFILE.clientFileExt),
                    imageGuid = FORM.imageId
                );
            </cfscript>   
            <cfset status = #ret#> 
            <cfif status neq "OK">
                <cfset message = "An error occured, please retry again.">
                <cfset disableButton = "">
            </cfif>     
        </cfif>  
	</cfif>        
<cfelse>
	<cfset message = "">
    <cfset disableButton = "">
    <cfset FORM.categoryId = URL.categoryId>
    <cfset type = URL.Type> 
    <cfset FORM.imageId = URL.imageId>
</cfif>

<cfinclude template="adminPage.cfm">



