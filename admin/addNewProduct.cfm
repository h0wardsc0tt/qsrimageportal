<cfsetting showdebugoutput="no">
<cfinclude template="../serverSettings.cfm">
<!--- Param form data. --->
<cfparam name="FORM.Page_UID" default="">
<cfparam name="FORM.id" type="numeric" default="0" />

<cfparam name="FORM.product" type="string" default="" />
<cfparam name="FORM.productId" type="string" default="" />
<cfparam name="FORM.productOrder" type="string" default="" />
<cfparam name="FORM.product_Order" type="string" default="" />
<cfparam name="FORM.imageType" type="string" default="" />
<cfparam name="FORM.imageAnchorText" type="string" default="" />
<cfparam name="FORM.productDescription" type="string" default="" />
<cfparam name="FORM.imageSort" type="string" default="" />
<cfparam name="FORM.imageSeq" type="string" default="" />
<cfparam name="FORM.description" type="string" default="No" />

<cfparam name="FORM.imageId" type="string" default="" />
<cfparam name="FORM.categoryId" type="string" default="" />
<cfparam name="FORM.submitted" type="boolean" default="false" />
<cfparam name="FORM.photo" type="string" default="" />
<cfparam name="FORM.imageDescription" type="string" default="" />
<cfparam name="FORM.type" type="string" default="" />
<cfparam name="deleteOption" type="string" default="No" />

<cfparam name="URL.imageId" default="">
<cfparam name="URL.type" default="">
<cfparam name="URL.categoryId" default="">

<cfset ProductOrder = "1">
<cfset Product = "1">
<cfset message = "Update Successful.">
<cfset disableButton = "disabled">
<cfset type = "">
<cfset title = ""> 
<cfset isActiveOptionText = "">   
<cfset imageType = "">
<cfset imageAnchorText = "">
<cfset productDescription = "">
<cfset imageSort = "1">
              
<cfif ReFindNoCase("([0-9A-Z]){32}", FORM.Page_UID) GT 0>
	<cfabort>
</cfif>

<cfscript>
	object = CreateObject("component", "#cfcs#.ImagePortal");  
</cfscript> 

<!--- Check to see if the form as been submitted. --->
<cfif FORM.submitted>
	<cfset type = FORM.type>
	<cfif Len( FORM.photo )>
		<cfset createThumbnail = false>
        <cfinclude template="../cfincludes/fileUpload.cfm">    
            
		<cfif type eq "addNewProduct" || type eq "addImageLink">  
        	<cfif type eq "addNewProduct">
            	<cfset imageType = "2">
                <cfset productDescription = #FORM.imageDescription#>
                <cfset ProductOrder = #FORM.productId#>
			<cfelse>
                <cfset productDescription = #FORM.productDescription#>
                <cfset ProductOrder = #FORM.product_Order# >
                <cfset imageSort = Form.imageSeq>
                
                <cfscript> 
                    object_LinkType = object.getLinkType(LCase(CFFILE.clientFileExt));
                </cfscript>
				<cfset imageAnchorText = #object_LinkType#> 
				<cfscript> 
                    object_ImageType = object.getImageType(imageAnchorText);
                </cfscript>
				<cfset imageType = #object_ImageType#>
			</cfif>
            <cfscript> 
                ret = object.AddNewProduct
                (
                  	image = binPhoto, 
					categoryGuid = FORM.categoryId,
                   	imageAnchorText = '',
               		imageName = CFFILE.clientFileName, 
                    imagePrefix = LCase(CFFILE.clientFileExt),
                    imageDescription = FORM.imageDescription
                );
            </cfscript>
            <cfset status = #ret#> 
            <cfif status neq "OK">
                <cfset message = "An error occured, please retry again.">
                <cfset disableButton = "">
            </cfif>
		<cfelse>
            <cfif type eq "thumbnail" || type eq "header">  
                <cfscript> 
                    ret = object.updateImageAndDescrption(image = binPhoto, imageGuid = FORM.imageId, imageDescription = FORM.imageDescription);
                </cfscript>
                <cfset status = #ret#> 
                <cfif status neq "OK">
					<cfset message = "An error occured, please retry again.">
                    <cfset disableButton = "">
                </cfif>
            <cfelse>
	            <cfscript> 
                    object_LinkType = object.getLinkType(LCase(CFFILE.clientFileExt));
                </cfscript>
                <cfset imageAnchorText = #object_LinkType#> 
                <cfscript> 
                    object_ImageType = object.getImageType(imageAnchorText);
                </cfscript>
                <cfset imageType = #object_ImageType#>            
                <cftry> 
                    <cfquery name="updateLinkImage" datasource="#APPLICATION.Datasource#" > 
                        update ImagePortal.Images 
                        set 
                            image = <cfqueryparam
                                    value="#binPhoto#"
                                    cfsqltype="cf_sql_blob"
                                    />,
                           	imageType = #imageType#, 
                            imageAnchorText = '#imageAnchorText#', 
                            imageName = '#CFFILE.clientFileName#', 
                            imagePrefix = '#LCase(CFFILE.clientFileExt)#'
                            where guid = '#FORM.imageId#'
                    </cfquery>
                    <cfcatch type="database">
                        <cfset message = "An error occured, please retry again.">
                        <cfset disableButton = "">
                    </cfcatch>
                </cftry>            
            </cfif>
        </cfif>
    <cfelse>
    	<cfif type neq "addNewProduct" && type neq "addImageLink">
        	<cfif type eq "thumbnail"|| type eq "header">           
                <cfscript> 
                    ret = object.updateImageDescription(imageGuid = FORM.imageId, imageDescription = FORM.imageDescription);
                </cfscript>
				<cfset status = #ret#> 
                <cfif status neq "OK">
					<cfset message = "An error occured, please retry again.">
                    <cfset disableButton = "">
                </cfif>
                          
            <cfelse>
            <!---
                <cftry>
                    <cfquery name="updateLink" datasource="#APPLICATION.Datasource#" > 
                        update ImagePortal.Images 
                        set 
                            ImageSort = #FORM.imageSort#
                            where guid = '#FORM.imageId#'
                    </cfquery>
                    <cfset message = "Update Successful">
                    <cfset disableButton = "disabled">
                    <cfcatch type="database">
                        <cfset message = "An error occured, please retry again.">
                        <cfset disableButton = "">
                    </cfcatch>
                </cftry> 
				--->              
            </cfif>
		</cfif>
    </cfif>
<cfelse>
    <cfset type =  URL.Type>
    <cfif type eq "addNewProduct">
    	<cftry>
            <cfquery name="addNewProduct" datasource="#APPLICATION.Datasource#" > 
  				SELECT 
      				max(i.Product)+1 as Product,
      				max(i.ProductOrder)+1 as ProductOrder
  				FROM ImagePortal.categories c
                INNER JOIN ImagePortal.images i on i.categoryId = c.categoryId
	  			WHERE c.CategoryGuid = '#URL.categoryId#'
            </cfquery> 
			<cfset ProductOrder = addNewProduct.ProductOrder>
			<cfset Product = addNewProduct.Product>
			<cfcatch type="database">
				<cfset ProductOrder = "1">
				<cfset Product = "1">
            </cfcatch>
        </cftry>
    </cfif>   
	<cfset message = "">
    <cfset disableButton = "">
    <cfif type neq "addNewProduct">
		<cfscript>  
            object_getImage = object.getImageById(URL.imageId);
        </cfscript>
		<cfset FORM.imageAnchorText = object_getImage.ImageAnchorText>
       	<cfset FORM.imageDescription = object_getImage.ImageDescription>
       	<cfset FORM.imageSort = object_getImage.ImageSort>
       	<cfset FORM.imageId = URL.imageId>
       	<cfset FORM.imageSeq = object_getImage.ImageCount + 1>
       	<cfset FORM.product_Order = object_getImage.ProductOrder>
       	<cfset Product = object_getImage.Product>
    </cfif>
   <cfset FORM.productOrder = ProductOrder>
   <cfset FORM.productId = Product>
   <cfset FORM.categoryId = URL.categoryId>
</cfif>

<cfinclude template="adminPage.cfm">



