<cfsilent>
<cfif SESSION.IsLoggedIn OR CGI.SCRIPT_NAME CONTAINS "logout.cfm"><!---2012/06/08 ATN: if user is already logged in forward them to home page --->
	<cflocation url="http://#CGI.HTTP_HOST#" addtoken="no">
	<cfabort>
</cfif>
<cfinclude template="/admin/security/_dat_prep_login.cfm">
</cfsilent>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Content Library - HME</title>
	<link type="text/css" href="/style.portal.css" rel="stylesheet" />
</head>

<body>
<div id="Content">
	<div class="disclaimer">
		<p><img src="/HME-logo-large.png" border="0" /></p>
		<p>All rights reserved.&nbsp;   No part of this website may be reproduced or transmitted in any form or by any   means without prior permission in writing from HME, Inc.</p>
	</div>
<cfif StructKeyExists(ERR, "loginmax")>
	<cfinclude template="./_tmp_loginMax.cfm">
<cfelse>
	<cfinclude template="./_tmp_loginForm.cfm">
</cfif>
</div>