<nav class="navbar navbar">
    <div class="container">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span> 
                </button>
                <a class="navbar-brand navbar-logo" href=""><img src="./images/HME-logo-large.png" alt="HME" <!---width="80"---> height="35" /></a>
                <a class="navbar-brand" href="#">PDF Manuals</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><span class="glyphicon glyphicon-cog"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="./?pg=ManageManuals"><span class="glyphicon glyphicon-file"></span> &nbsp;User Manuals</a></li>
                			<li><a href="./?pg=ManageManuals&st=Edit"><span class="glyphicon glyphicon-user"></span> &nbsp;Admin</a></li>
                    		<li><a href="./?pg=Logout"><span class="glyphicon glyphicon-log-out"></span> &nbsp;Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

