<!--- Retrive Image from Database, passing image ImageId in query parameter --->
<cfsetting showdebugoutput="no">
<cfparam name="URL.id" default="0">
<cfinclude template="./cfincludes/userFunctions.cfm">
<cfscript>	
	object = CreateObject("component", "_cfcs.ImagePortal");  
	object_getImage = object.getImageById(URL.id);
</cfscript>

<cfif #object_getImage.ImageType# NEQ "tif">
	<cfheader name="Content-Disposition" value="attachment; 
    	filename=#replace(object_getImage.ImageName," ","_","ALL")#.#object_getImage.ImagePrefix#">
	<cfcontent reset = "Yes" type = "image/#object_getImage.ImageType#" variable = "#object_getImage.Image#" />
<cfelse> 
	<cfheader name="Content-Disposition" value="attachment; 
    	filename=#replace(object_getImage.ImageName," ","_","ALL")#.#object_getImage.ImagePrefix#">
	<cfcontent reset = "Yes" type = "image/tif; cht=iso-8859-1" variable = "#object_getImage.Image#" />
</cfif>
    
  


