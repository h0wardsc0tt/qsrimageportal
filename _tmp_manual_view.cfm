<cfoutput>
    <div class="container container-main">
        <h2>HME User Manuals</h2>		
        <cfloop from="1" to="#objCategories.RecordCount#" index="Category">
            <h3>#objCategories.Category_Name[Category]#</h3>
            <div class="panel-group" id="accordion_#Category#" role="tablist" aria-multiselectable="true">
                <cfloop from="1" to="#objProducts.RecordCount#" index="Product">
                	<cfif objCategories.Category_ID[Category] EQ objProducts.Product_Category_ID[Product]>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h3 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="##accordion_#Category#" href="##collapse_#Product#" aria-expanded="false" aria-controls="collapse_#Product#">&nbsp;#objProducts.Product_Name[Product]#</a>
                                </h3>
                            </div>
                            <div id="collapse_#Product#" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_#Product#" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <ul class="pdf-list">
                                        <cfloop from="1" to="#objManuals.RecordCount#" index="Manual">
                							<cfif objManuals.Manual_Product_ID[Manual] EQ objProducts.Product_ID[Product]>
                                            	<li><a href="http://#APPLICATION.rootURL##objManuals.Manual_File_Path[Manual]#" target="_blank">#objManuals.Manual_Label[Manual]#</a></li>
                                            </cfif>
                                        </cfloop>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </cfif>
                </cfloop>
                <br />
            </div>
        </cfloop>
    </div>
</cfoutput>
