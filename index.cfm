
<cfsetting showdebugoutput="no">

<cfparam name="FORM.Search" type="string" default="" />
<cfparam name="FORM.submitted" type="boolean" default="false" />

<cfparam name="URL.pg" default="">
<cfparam name="URL.st" default="">

<cfset adminRights = false>

<!---<cfdump var="#SESSION#">--->

<cfif StructKeyExists(SESSION,"IsLoggedIn")>
	<cfif SESSION.User_Name neq "HME">
        <cfset adminRights =  SESSION.IsLoggedIn>
    </cfif>
</cfif>

<cfinclude template="./cfincludes/userFunctions.cfm">
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<title>HME Image Portal</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!-- Custom styles for this template -->
<link href="css/dashboard.css" rel="stylesheet">
<link href="css/qsrImagePortal.css" rel="stylesheet">

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand navbar-brand-hme-image nav-logo" href="#"><img src="./images/hme-logo.png" alt="HME Logo" class="img-responsive"></a> </div>
    <div id="navbar" class="navbar-collapse collapse">
	  <ul class="nav navbar-nav navbar-right">
      <cfif not adminRights>
		<li><a href="admin.cfm?pg=Login">Login</a></li>
      <cfelse>
		<li><a href="admin.cfm?pg=Logout">Logout</a></li>
      </cfif>
	  </ul>
      <form method="post" class="navbar-form navbar-right" id="SearchForm">
      	<div class="search">
      		<span class="fa fa-search" onClick="$('#SearchForm').submit();"></span>
        	<input type="search" id="Search" name="Search" class="form-control" placeholder="Search..." >
		</div>
		<input type="hidden" name="submitted" value="true" />
      </form>
    </div>
  </div>
</nav>
<div class="container-fluid" style="min-height: 100%;position:relative;">
	<a name="top"></a>
	<div class="row placeholders" style="display:none;"> <img src="images/header.jpg" border="0" alt="PRO AUDIO GRAPHIC RESOURCES" class="img-responsive" /></div>
    
	<h4 style="text-align:center; font-weight:bold;">QSR GRAPHIC RESOURCES</h4>
    <div style=" text-align:center">The purpose of this portal is to assist dealers and distributors to promote the sale of HME products. Prior to publicly using any content from this portal, you must obtain written permission from HME, Inc. All rights reserved. No part of this website may be reproduced or transmitted in any form or by any means without prior permission in writing from HME, Inc.</div>
    <div style=" text-align:center"><a href="HME_Legal_Guidelines_QSR.pdf">
        <img src="images/HMETrademark.jpg" width="304" height="auto" border="0" ></a>
    </div>
    
    <div style=" text-align:center">All rights reserved. No part of this website may be reproduced or transmitted in any form or by any means without prior permission in writing from HME, Inc.</div>

	<cfscript>
        objectCategories = CreateObject("component", "_cfcs.ImagePortal"); 
    </cfscript>
    <cfif FORM.submitted && Len( FORM.Search )>
        <cfscript>
            object_getCategoriesAndProductImages = objectCategories.SearchCategoriesAndProductImages(#FORM.Search#);
        </cfscript>
    <cfelse>
        <cfscript>
            object_getCategoriesAndProductImages = objectCategories.getCategoriesAndProductImages();
        </cfscript>
    </cfif>
    
	<cfset count = 1>
    <cfset productCount = 1>
    <cfset anchorlist_1 = "">
    <cfset anchorlist_2 = "">
    
	<cfoutput query="object_getCategoriesAndProductImages" group="categoryID">
    	<cfif count MOD 2>
   			<cfset anchorlist_1 = anchorlist_1 & "<a href='##" & replaceNoCase(replaceNoCase(#categoryName#, " ", "", "ALL"), "|", "", "ALL") & "' class='listLink'>#categoryDescription#</a><br>">
        <cfelse>
   			<cfset anchorlist_2 = anchorlist_2 & "<a href='##" & replaceNoCase(replaceNoCase(#categoryName#, " ", "", "ALL"), "|", "", "ALL") & "' class='listLink'>#categoryDescription#</a><br>">
        </cfif>		
		<cfset count = count + 1>
    </cfoutput>
	<cfset count = 0>
	<div style="text-align:center;vertical-align:top;" class="linkholder">
        <div  style="text-align:left;display:inline-block; vertical-align:top; border-bottom: solid 1px #f5f5f5;">
            <div style="text-align:center;vertical-align:top;" id="anchorLinks" class="expandable-element"> 
                <div id="anchorLink-1" style="text-align:left;display:inline-block; vertical-align:top;"> 
                    <cfoutput>#anchorlist_1#</cfoutput>
                </div>		
                <div id="anchorLink-2" style="text-align:left;display:inline-block; vertical-align:top;"> 
                    <cfoutput>#anchorlist_2#</cfoutput>
                </div>
            </div>
		</div>
            <a href="#"><i id="anchroArrow" class="fa fa-minus" title="Collapse Links"  onClick="anchorLinks();"></i></a> 
	</div>
  	<div class="row">
		<cfif adminRights>
			Categories:&nbsp;&nbsp;
            <a href="javascript:newCategory('newCategory', 'newCategory')"><i class="fa fa-plus" title="Add a New Category"></i></a>
            &nbsp;&nbsp;<a href="javascript:restoreCategory('restoreCategory')"><i class="fa fa-refresh" title="Restore a Deleted Category"></i></a>
            &nbsp;&nbsp;<a href="javascript:orderCategories('orderCategory')"><i class="fa fa-sort" title="Order Categories"></i></a>
            <br /><br /><br />
        </cfif>
    </div>
	<cfoutput query="object_getCategoriesAndProductImages" group="categoryID">            
            <div class="row">
                <div> 
                    <a name="#replaceNoCase(replaceNoCase(categoryName, " ", "", "ALL"), "|", "", "ALL")#" 
                    	id="#replaceNoCase(replaceNoCase(categoryName, " ", "", "ALL"), "|", "", "ALL")#">
                    <img src="./getimage.cfm?id=#guid#" class="img-responsive" style="width:491px; height:auto; border:0"
                    	title="#categoryDescription#"/></a> 
                    <cfif adminRights>
                    	Edit Category Header:&nbsp;&nbsp;<a href="javascript:editCategory('category','#guid#', '#CategoryGuid#', '#CategoryName#', '#CategoryDescription#')">
                    	<i class="fa fa-pencil-square-o" title="Edit Category"></i></a><br /> <br />
                    	Products:&nbsp;&nbsp;<a href="javascript:newProduct('addNewProduct', '#CategoryGuid#')"><i class="fa fa-plus" title="Add a New Product"></i></a>
                    	&nbsp;&nbsp;<a href="javascript:restoreProduct('restoreProduct', '#CategoryGuid#')"><i class="fa fa-refresh" title="Restore Deleted Product"></i></a>
                        &nbsp;&nbsp;<a href="javascript:orderProduct('reOrderProduct', '#CategoryGuid#')"><i class="fa fa-sort" title="Order Products"></i></a>
                    </cfif>                    
                </div>
            </div>
            <div class="row placeholders">
            <cfset productCount = 1>
 		    <cfoutput group="product"> 
           		<cfset count = 0> 
            	<cfif imageCount gt 0>
	                <cfoutput group="imageId">          
	                    <cfswitch expression="#imageType#"> 
	                        <cfcase value=1>
	             
	                        </cfcase>
	                        <cfcase value=2>
	                            <cfif productCount MOD 3 eq 0>
	                                <div class="clearfix visible-xs"></div>
	                            </cfif>
	                            <div class="col-xs-6 col-sm-3 placeholder"> 
	                            <img id="img1" src="./getimage.cfm?id=#guid#" width="100px" height="auto" 
	                            class="img-responsive" title="#TRIM(ImageDescription)#"/>
	                            <cfif adminRights>
		                            <div>	                            
		                            Edit Thumbnail:&nbsp;&nbsp;<a href="javascript:editThumbnail('thumbnail','#guid#', '#CategoryGuid#', '#TRIM(ImageDescription)#', 'thumbnail')">
		                            <i class="fa fa-pencil-square-o" title="Edit Thumbnail"></i></a> 
									</div>
	                            </cfif>
	                            <h5>#imagedescription#</h5>
	                            <cfset count = count + 1>
	                            <cfif productCount MOD 4 eq 0>
	                                <cfset productCount = 1>
	                            <cfelse>
	                                <cfset productCount = productCount + 1>
	                            </cfif>
	                        </cfcase>
	                        <cfcase value=3>
	                            <div>
	                                <cfif adminRights>
	                                <a href="javascript:editImage('#TRIM(ImagePrefix)#','#guid#', '#CategoryGuid#', 'downloadLinks')">
	                                <i class="fa fa-pencil-square-o" title="Edit Image Link"></i></a>
	                                </cfif>
	                                <a href="javascript:displayImage('#guid#','#TRIM(ImageName)#.#TRIM(ImagePrefix)#','#TRIM(ImageName)#');" 
	                                    class="listLink"><div class="product-links">#TRIM(imageanchortext)#</div></a>
	                            </div>        
	                            <cfset count = count + 1>
	                        </cfcase>
	                        <cfcase value=4>
	                            <div>
	                                <cfif adminRights>
	                                <a href="javascript:editImage('#TRIM(ImagePrefix)#','#guid#', '#CategoryGuid#', 'downloadLinks')">
	                                <i class="fa fa-pencil-square-o" title="Edit Image Link"></i></a>
	                                </cfif>
	                                <a href="./getimage.cfm?id=#guid#" 
	                                    download="#TRIM(ImageName)#.#TRIM(ImagePrefix)#" 
	                                    class="listLink"><div class="product-links">#TRIM(imageanchortext)#</div></a>
	                            </div>                             
	                            <cfset count = count + 1>
	                        </cfcase>                
	                    </cfswitch>	 
	                    <cfif count eq imageCount>
                            <cfif adminRights>
                            	Image Links:
                            <a href="javascript:editImage('addImageLink','#guid#', '#CategoryGuid#', 'addNewProductLink')">
                            <i class="fa fa-plus" title="Add an Image Link"></i> </a>
                        	&nbsp;&nbsp;<a href="javascript:restoreLink('restoreLinks', '#product#', '#CategoryGuid#', '#Guid#')">
                        	<i class="fa fa-refresh" title="Restore a deleted Link"></i> </a>
                        	&nbsp;&nbsp;<a href="javascript:orderLinks('orderLinks', '#product#', '#CategoryGuid#')">
                        	<i class="fa fa-sort" title="Order Links"></i> </a>
                            </cfif>
	                        </div>
	                    </cfif>
	                </cfoutput>
	             </cfif> 
	            </cfoutput>
            <cfif count neq 0>
             	</div> 
             	<div style="position:relative; width:200px; text-align:center;">           
                 	<a href="##top" class="listLink" style="position:absolute; top:-50px;">Back To Top</a>
             	</div>
             </cfif>
   </cfoutput>
</div>

<!-- Modal -->
<div id="imgModal" class="modal fade" role="dialog"  data-backdrop="static" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 id="imageTitle" class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <p align="center" >
        	<img id="modalImage" class="img-responsive" style="max-height:300px; width:auto;" />
        </p>
        <p class="align-center">
        	<a id="imageDownload" >Download Image</a>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="EditCategory" class="modal fade" role="dialog"  data-backdrop="static" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 id="imageTitle" class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <p align="center" >
        	<iframe id="adminEdit" frameborder="0"> </iframe>
        </p>
      </div>
      <div class="modal-footer">
        <button id="modalSubmitBtn" type="button" class="btn btn-primary" value="Submit" onClick="$('#adminEdit')[0].contentWindow.submitForm();">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="confirm" class="modal fade" role="dialog"  data-backdrop="static" style="z-index:999999" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 id="imageTitle" class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <p align="center" >
        	ddddddddddddddddddd
        </p>
      </div>
      <div class="modal-footer">
        <button id="modalSubmitBtn" type="button" class="btn btn-primary" value="Submit" onClick="$('#adminEdit')[0].contentWindow.submitForm();">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<script src="js/qsrImagePortal.js" type="text/javascript"></script>
<style type="text/css">

</style>

</body>
</html>
