<!--- Upload the file. --->
<cffile
    action="upload"
    filefield="photo"
    destination="#ExpandPath( './' )#"
    nameconflict="makeunique"
    />

<cfif #createThumbnail#>
	<cfset imageQuality = 0.95 />
	<cfimage 
		name="photo" 
		action="read" 
		source="#ExpandPath( './' )##CFFILE.ServerFile#"
	    /> 
	
	
	<!--- Get image and resize it. ---> 
	<cfset thumbnail=ImageNew("#ExpandPath( './' )##CFFILE.ServerFile#")> 
	<cfset ImageResize(thumbnail,"100","")> 
	
	<!--- Save the modified image to a file. ---> 
	<cfimage 
		source="#thumbnail#" 
		action="write" 
		destination="#ExpandPath( './' )##CFFILE.ServerFile#" 
		overwrite="yes"
		quality="#imageQuality#"
		>
</cfif>

  
<!--- Read in the binary data. --->
<cffile
    action="readbinary"
    file="#ExpandPath( './' )##CFFILE.ServerFile#"
    variable="binPhoto"
    />

<!--- Delete photo from server. --->
<cffile
    action="delete"
    file="#ExpandPath( './' )##CFFILE.ServerFile#"
    />
