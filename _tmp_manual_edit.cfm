<cfoutput>

	<!--- ADD MANUAL --->
    <div class="container container-main view-cont manual-cont manual-add-cont">
        <form action="" method="POST" name="Add_Manual_Form" id="Add_Manual_Form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="upload-head"><span class="glyphicon glyphicon-plus"></span> Add new manual <span class="glyphicon glyphicon-edit pull-right manual-edit" onClick="toggleItem('manual','Edit');"></span></h4>
                    <div class="upload-cont"><span class="glyphicon glyphicon-upload"></span> &nbsp;<span class="upload-label">click here to select pdf</span></div>
                    <input type="file" name="Manual_File" id="Manual_File" required="required"/>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-5">
                    <label for="Manual_Product_ID">Product:</label>
                    <select class="form-control" id="Manual_Product_ID" name="Manual_Product_ID" required="required">
                    	<option value=""></option>
                        <cfloop from="1" to="#objProductsOrd.RecordCount#" index="Product">
                            <option value="#objProductsOrd.Product_ID[Product]#">#objProductsOrd.Product_Name[Product]#</option>
                        </cfloop>
                    </select>
                </div>
                <div class="form-group col-lg-5">
                    <label for="Manual_Label">Manual Name:</label>
                    <input type="text" class="form-control" id="Manual_Label" name="Manual_Label" required="required">
                </div>
                <div class="col-lg-2"></div>
                <div class="form-group col-lg-5">
                    <label for="Manual_IsExternal">Accessability:</label>
                    <select class="form-control" id="Manual_IsExternal" name="Manual_IsExternal" required="required">
                    	<option value=""></option>
                        <option value="1">Internal &amp; External</option>
                        <option value="0">Internal (Only)</option>
                    </select>
                </div>
                <div class="form-group col-lg-5">
                    <label for="Manual_Part_Number">Part Number:</label>
                    <input type="text" class="form-control" id="Manual_Part_Number" name="Manual_Part_Number" required="required">
                </div>
                <div class="form-group col-lg-2 pull-right">
                	<label for="Add_Manual">&nbsp;</label>
                	<input type="submit" id="Add_Manual" name="Add_Manual" class="btn btn-default btn-save form-control" value="Add" onClick="addItem('Manual');" />
                </div>
            </div>
        </form>
    </div>
    
    <!--- EDIT MANUAL --->
    <div class="container container-main view-cont manual-cont manual-edit-cont">
        <form action="" method="POST" name="Edit_Manual_Form" id="Edit_Manual_Form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="upload-head"><span class="glyphicon glyphicon-edit"></span> Edit manual <span class="glyphicon glyphicon-plus pull-right manual-add" onClick="toggleItem('manual','Add');"></span><span class="glyphicon glyphicon-remove pull-right Manual-remove" onClick="removeItemConfirm('Manual');"></span></h4>
                	<div class="upload-cont-edit"><span class="glyphicon glyphicon-upload"></span> &nbsp;<span class="upload-label-edit">click here to select pdf</span></div>
                    <input type="file" name="Manual_File_Edit" id="Manual_File_Edit"/>
                </div>
            </div>
            <div class="row">
            	<div class="form-group col-lg-5">
                    <label for="Manual_Manual_ID">Select Manual by Name:</label>
                    <select class="form-control select-item smbn" id="Manual_Manual_ID" name="Manual_Manual_ID" required="required">
                    	<option value=""></option>
                        <cfloop from="1" to="#objManualsOrd.RecordCount#" index="Manual">
                            <option value="#objManualsOrd.Manual_ID[Manual]#">#objManualsOrd.Manual_Label[Manual]#</option>
                        </cfloop>
                    </select>
                </div>
                <div class="form-group col-lg-5">
                    <label for="Manual_Manual_ID">Select Manual by P/N:</label>
                    <select class="form-control select-item smbp" id="Manual_Manual_ID" name="Manual_Manual_ID" required="required">
                    	<option value=""></option>
                        <cfloop from="1" to="#objManualsOrdPN.RecordCount#" index="Manual">
                            <option value="#objManualsOrdPN.Manual_ID[Manual]#">#objManualsOrdPN.Manual_Part_Number[Manual]#</option>
                        </cfloop>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-5">
                    <label for="Manual_Product_ID_Edit">Product:</label>
                    <select class="form-control" id="Manual_Product_ID_Edit" name="Manual_Product_ID_Edit" required="required">
                    	<option value=""></option>
                        <cfloop from="1" to="#objProductsOrd.RecordCount#" index="Product">
                            <option value="#objProductsOrd.Product_ID[Product]#">#objProductsOrd.Product_Name[Product]#</option>
                        </cfloop>
                    </select>
                </div>
                <div class="form-group col-lg-5">
                    <label for="Manual_Label_Edit">Manual Name:</label>
                    <input type="text" class="form-control" id="Manual_Label_Edit" name="Manual_Label_Edit" required="required">
                </div>
                <div class="col-lg-2"></div>
                <div class="form-group col-lg-5">
                    <label for="Manual_IsExternal_Edit">Accessability:</label>
                    <select class="form-control" id="Manual_IsExternal_Edit" name="Manual_IsExternal_Edit" required="required">
                    	<option value=""></option>
                        <option value="1">Internal &amp; External</option>
                        <option value="0">Internal (Only)</option>
                    </select>
                </div>
                <div class="form-group col-lg-5">
                    <label for="Manual_Part_Number_Edit">Part Number:</label>
                    <input type="text" class="form-control" id="Manual_Part_Number_Edit" name="Manual_Part_Number_Edit" required="required">
                </div>
                <div class="form-group col-lg-2 pull-right">
                	<label for="Add_Manual">&nbsp;</label>
                    <input type="hidden" id="Manual_UID" name="Manual_UID" value="" />
                	<input type="submit" id="Add_Manual" name="Add_Manual" class="btn btn-default btn-save form-control" value="Save" onClick="editItem('Manual');" />
                </div>
            </div>
        </form>
    </div>
    
    <!--- ADD PRODUCT --->
    <div class="container container-main view-cont product-cont product-add-cont">
        <form action="" method="POST" name="Add_Product_Form" id="Add_Product_Form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="upload-head"><span class="glyphicon glyphicon-plus"></span> Add new product <span class="glyphicon glyphicon-edit pull-right product-edit" onClick="toggleItem('product','Edit');"></span></h4>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-5">
                    <label for="Product_Category_ID">Category:</label>
                    <select class="form-control" id="Product_Category_ID" name="Product_Category_ID" required="required">
                    	<option value=""></option>
                        <cfloop from="1" to="#objCategoriesOrd.RecordCount#" index="Category">
                            <option value="#objCategoriesOrd.Category_ID[Category]#">#objCategoriesOrd.Category_Name[Category]#</option>
                        </cfloop>
                    </select>
                </div>
                <div class="form-group col-lg-5">
                    <label for="Product_Name">Name:</label>
                    <input type="text" class="form-control" id="Product_Name" name="Product_Name" required="required">
                </div>
                <div class="col-lg-2"></div>
                <div class="form-group col-lg-5">
                    <label for="Product_Desc">Description:</label>
                    <input type="text" class="form-control" id="Product_Desc" name="Product_Desc">
                </div>
                <div class="form-group col-lg-2 pull-right">
                	<label for="Add_Product">&nbsp;</label>
                	<input type="submit" id="Add_Product" name="Add_Product" class="btn btn-default btn-save form-control" value="Add" onClick="addItem('Product');" />
                </div>
            </div>
        </form>
    </div>
    
    <!--- EDIT PRODUCT --->
    <div class="container container-main view-cont product-cont product-edit-cont">
        <form action="" method="POST" name="Edit_Product_Form" id="Edit_Product_Form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="upload-head"><span class="glyphicon glyphicon-edit"></span> Edit product <span class="glyphicon glyphicon-plus pull-right product-add" onClick="toggleItem('product','Add');"></span><span class="glyphicon glyphicon-remove pull-right Product-remove" onClick="removeItemConfirm('Product');"></span></h4>
                </div>
            </div>
            <div class="row">
            	<div class="form-group col-lg-5">
                    <label for="Product_Product_ID">Select Product:</label>
                    <select class="form-control select-item" id="Product_Product_ID" name="Product_Product_ID" required="required">
                    	<option value=""></option>
                        <cfloop from="1" to="#objProductsOrd.RecordCount#" index="Product">
                            <option value="#objProductsOrd.Product_ID[Product]#">#objProductsOrd.Product_Name[Product]#</option>
                        </cfloop>
                    </select>
                </div>
                <div class="form-group col-lg-5">
                    <label for="Product_Category_ID_Edit">Category:</label>
                    <select class="form-control" id="Product_Category_ID_Edit" name="Product_Category_ID_Edit" required="required">
                    	<option value=""></option>
                        <cfloop from="1" to="#objCategoriesOrd.RecordCount#" index="Category">
                            <option value="#objCategoriesOrd.Category_ID[Category]#">#objCategoriesOrd.Category_Name[Category]#</option>
                        </cfloop>
                    </select>
                </div>
                <div class="form-group col-lg-5">
                    <label for="Product_Name_Edit">Name:</label>
                    <input type="text" class="form-control" id="Product_Name_Edit" name="Product_Name_Edit" required="required">
                </div>
                <div class="form-group col-lg-5">
                    <label for="Product_Desc_Edit">Description:</label>
                    <input type="text" class="form-control" id="Product_Desc_Edit" name="Product_Desc_Edit">
                </div>
                <div class="form-group col-lg-2 pull-right">
                	<label for="Add_Product">&nbsp;</label>
                    <input type="hidden" id="Product_UID" name="Product_UID" value="" />
                	<input type="submit" id="Add_Product" name="Add_Product" class="btn btn-default btn-save form-control" value="Save" onClick="editItem('Product');" />
                </div>
            </div>
        </form>
    </div>
    
    <!--- ADD CATEGORY --->
    <div class="container container-main view-cont category-cont category-add-cont">
        <form action="" method="POST" name="Add_Category_Form" id="Add_Category_Form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="upload-head"><span class="glyphicon glyphicon-plus"></span> Add new category <span class="glyphicon glyphicon-edit pull-right category-edit" onClick="toggleItem('category','Edit');"></span></h4>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-5">
                    <label for="Category_Name">Name:</label>
                    <input type="text" class="form-control" id="Category_Name" name="Category_Name" required="required">
                </div>
                <div class="form-group col-lg-5">
                    <label for="Category_Desc">Description:</label>
                    <input type="text" class="form-control" id="Category_Desc" name="Category_Desc">
                </div>
                <div class="form-group col-lg-2 pull-right">
                	<label for="Add_Category">&nbsp;</label>
                	<input type="submit" id="Add_Category" name="Add_Category" class="btn btn-default btn-save form-control" value="Add" onClick="addItem('Category');" />
                </div>
            </div>
        </form>
    </div>
    
    <!--- EDIT CATEGORY --->
    <div class="container container-main view-cont category-cont category-edit-cont">
        <form action="" method="POST" name="Edit_Category_Form" id="Edit_Category_Form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="upload-head"><span class="glyphicon glyphicon-edit"></span> Edit category <span class="glyphicon glyphicon-plus pull-right category-add" onClick="toggleItem('category','Add');"></span><span class="glyphicon glyphicon-remove pull-right Category-remove" onClick="removeItemConfirm('Category');"></span></h4>
                </div>
            </div>
            <div class="row">
				<div class="form-group col-lg-5">
                    <label for="Category_Category_ID">Select Category:</label>
                    <select class="form-control select-item" id="Category_Category_ID" name="Category_Category_ID" required="required">
                    	<option value=""></option>
                        <cfloop from="1" to="#objCategoriesOrd.RecordCount#" index="Category">
                            <option value="#objCategoriesOrd.Category_ID[Category]#">#objCategoriesOrd.Category_Name[Category]#</option>
                        </cfloop>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-5">
                    <label for="Category_Name_Edit">Name:</label>
                    <input type="text" class="form-control" id="Category_Name_Edit" name="Category_Name_Edit" required="required">
                </div>
                <div class="form-group col-lg-5">
                    <label for="Category_Desc_Edit">Description:</label>
                    <input type="text" class="form-control" id="Category_Desc_Edit" name="Category_Desc_Edit">
                </div>
                <div class="form-group col-lg-2 pull-right">
                	<label for="Add_Category">&nbsp;</label>
                    <input type="hidden" id="Category_UID" name="Category_UID" value="" />
                	<input type="submit" id="Add_Category" name="Add_Category" class="btn btn-default btn-save form-control" value="Save" onClick="editItem('Category');" />
                </div>
            </div>
        </form>
    </div>
    
    <!--- SUCCESS MODAL --->
    <div id="Add_Item_Modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                	<button type="button" class="close close-btn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-ok-circle"></span> Success!</h4>
                    <div class="modal-title-cont"></div>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
    
    <!--- WARNING MODAL --->
    <div id="Warning_Modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                	<button type="button" class="close close-btn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-exclamation-sign"></span> Warning!</h4>
                    <div class="modal-title-cont"></div>
                    <p></p>
                    <div class="col-lg-12">
                    	<input type="hidden" id="Remove_Item_Type" name="Remove_Item_Type" value="" />
                    	<input type="hidden" id="Remove_Item_UID" name="Remove_Item_UID" value="" />
                    	<button type="button" id="Remove_Item_Confirm" name="Remove_Item_Confirm" class="btn btn-default btn-save">Confirm</button>
                    	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</cfoutput>